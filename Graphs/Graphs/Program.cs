﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class Program
    {
        enum GraphEnum
        {
            Kuiv,
            Zutomur,
            White_chorch,
            Prulyku,
            Novograd,
            Berduchiv,
            Shepetowka,
            Uman,
            Cherkasy,
            Poltava,
            Symu,
            Murgorod,
            Rowno,
            Luck,
            Wunnuca,
            Hmelnuckii,
            Ternopul,
            Kremenchug,
            Harkow
        }

        static bool[] visited;
        static Queue<int> queue = new Queue<int>();

        static int dfs(int[][] graf, string name)
        {
            visited = new bool[graf.Length];
            visited.Initialize();  

            return dfsSearch(graf, name);
        }

        static int dfsSearch(int[][] graf, string name, int startI = 0)
        {            
            for (int i = startI; i < graf.Length; i++)
            {
                if (Enum.GetName(typeof(GraphEnum), i) == name)
                    return i;

                visited[i] = true;

                for (int j = 0; j < graf.Length; j++)
                {
                    if (graf[i][j] != 0 && visited[j] == false)
                    {
                        int result = dfsSearch(graf, name, j);
                        if (result != -1)
                            return result;
                    }
                }               
            }

            return -1;
        }


        static int bfs(int[][] graf, string name)
        {
            visited = new bool[graf.Length];
            visited.Initialize();

            queue.Clear();

            return bfsSearch(graf, name);
        }

        static int bfsSearch(int[][] graf, string name, int startI = 0)
        {
            for (int i = startI; i < graf.Length; i++)
            {
                if (Enum.GetName(typeof(GraphEnum), i) == name)
                    return i;

                visited[i] = true;

                for (int j = 0; j < graf.Length; j++)
                {
                    if (graf[i][j] != 0 && visited[j] == false)
                        queue.Enqueue(j);                        
                }

                while (queue.Count > 0)
                {
                    int index = queue.Dequeue();
                    if(!visited[index])
                        return bfsSearch(graf, name, index);
                }
            }

            return -1;
        }

        static void showRoutes(int[][] graf, int i = 0, string route = "", int lenght = 0)
        {
            route += Enum.GetName(typeof(GraphEnum), i);
            bool isContinue = false;

            for (int j = 0; j < graf.Length; j++)
            {
                if (graf[i][j] != 0) 
                {
                    showRoutes(graf, j, route + " - ", lenght + graf[i][j]);
                    isContinue = true;
                }
            }

            if(!isContinue)
                Console.WriteLine(route + "(" + lenght.ToString() + ")");
        }

        static void Main(string[] args)
        {
            int[][] graf = {
                new int[] { 0,135, 78, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Kuiv,1          
                new int[] { 135, 0, 0, 0, 0, 80, 38, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Zutomur,2          
                new int[] { 78, 0, 0, 0, 0, 0, 0, 115, 146, 181, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //White_chorch,3        
                new int[] { 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 109, 0, 0, 0, 0, 0, 0, 0}, //Prulyku,4        
                new int[] { 0, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0}, //Novograd,5         
                new int[] { 0, 38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0}, //Berduchiv,6           
                new int[] { 0, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Shepetowka,7       
                new int[] { 0, 0, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Uman,8     
                new int[] { 0, 0, 146, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 0}, //Cherkasy,9     
                new int[] { 0, 0, 181, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 130}, //Poltava,10  
                new int[] { 0, 0, 0, 175, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Symu,11
                new int[] { 0, 0, 0, 109, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Murgorod,12
                new int[] { 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 0, 0, 0, 0}, //Rowno,13
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 0, 0, 0, 0, 0, 0}, //Luck,14
                new int[] { 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0}, //Wunnuca,15
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 104, 0, 0}, //Hmelnuckii,16
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104, 0, 0, 0}, //Ternopul,17            
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // Kremenchug,18
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 130, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Harkow,19
                };

            int[][] grafO = {
                new int[] { 0,135, 78, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Kuiv,1          
                new int[] { 0, 0, 0, 0, 0, 80, 38, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Zutomur,2          
                new int[] { 0, 0, 0, 0, 0, 0, 0, 115, 146, 181, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //White_chorch,3        
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 109, 0, 0, 0, 0, 0, 0, 0}, //Prulyku,4        
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0}, //Novograd,5         
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0}, //Berduchiv,6           
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Shepetowka,7       
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Uman,8     
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 0}, //Cherkasy,9     
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 130}, //Poltava,10  
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Symu,11
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Murgorod,12
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 0, 0, 0, 0}, //Rowno,13
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Luck,14
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0}, //Wunnuca,15
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104, 0, 0}, //Hmelnuckii,16
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Ternopul,17            
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // Kremenchug,18
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //Harkow,19
                };

            Console.WriteLine( dfs(grafO, "Ternopul"));
            Console.WriteLine( bfs(grafO, "Ternopul"));
            showRoutes(grafO);
            Console.ReadLine();
        }
    }
}
